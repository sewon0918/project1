# STORYBOOTH #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

This project STORYBOOTH is a simple photo editor using text. I was inspired by the story function of instagram. I realized that we can only post stories in mobile, not in desktop. So I wanted to make desktop application that has similar function to story function in Instagram.
The name STORYBOOTH is combination of STORY + BOOTH of photobooth.

When this application run, camera first turns on. When camera button on the bottom is pressed, the video at that moment is captured. Then, text generator button "Aa" appears on the top-right. You can add a text by pressing "Aa". You can stop writing by pressing ENTER. You can change the size, font, and color of text any time, but you can rotate it only when you finish writing. 
These are the way to use functions of STORYBOOTH.

1. size
Slider on the left is for changing size. Default text size is 30, and you can change in the range of 20-150. 
2. rotation
Slider on the right is for rotation.
You can rotate your text 0-360 degrees.
3. font
The left small square button is for font. When you press the button, 20 different fonts appear, and you can select the font you want. 
4. color
The right small square button is for color. When you press the button, you can change the color of the text by changing each r, g, b value. 
5. modify
If you want to modify text that is already written, you can modify them by simply double click the text.
6. delete
When you drag the text and release it on the trashcan figure on the bottom-right, you can delete the text.

You can do these things with multiple texts. If you want to take photo again, just press camera button. Then all texts you added will be removed.


I used video library.
Video library provides reflected image. To make it show mirrored image, I refered to the souce code in 
https://processing.org/tutorials/video/





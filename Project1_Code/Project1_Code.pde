/*
	Project 1
	Name of Project: 
	Author: Sewon Lim
	Date: 200514 - 
*/


import processing.video.*;

int padding = 80;
int buttonsize = 50;
int videoScale = 1;
int cols, rows;
Capture video;
TextButton aa;
CameraButton cb;
DeleteButton db;
ArrayList<Text> textArr;
Text text;
PImage myImage;

String[] fontList;
private ArrayList<EachFont> fonts;
FontSelection fontselection;
// ColorSelection colorselection;

void setup() {  
	size(640, 480);
	fontList = new String[]
	{"Andale Mono.ttf", "Arial Bold.ttf", "BlackoakStd.otf", "Bodoni 72 Smallcaps Book.ttf", "Chalkduster.ttf", 
	"CharlemagneStd-Bold.otf", "Comic Sans MS Bold.ttf", "Courier New Bold.ttf", "Courier New.ttf", "HoboStd.otf", 
	"LetterGothicStd-Bold.otf", "LithosPro-Black.otf", "LithosPro-Regular.otf", "MinionPro-Bold.otf", "MyriadArabic-Bold.otf",
	"OCRAStd.otf", "OratorStd.otf", "PoplarStd.otf", "PrestigeEliteStd-Bd.otf", "TektonPro-Bold.otf"};
	
	println(fontList.length);
	int a = width - buttonsize/2;
	int b = buttonsize;
	aa = new TextButton(width - buttonsize/2, 3*buttonsize/4, buttonsize);
	cb = new CameraButton(width/2, height-buttonsize/2, buttonsize);
	db = new DeleteButton(width-buttonsize/2, height-2*buttonsize/3, buttonsize);

	textArr = new ArrayList<Text>(); 
	fonts = new ArrayList<EachFont>();
	fontselection = new FontSelection(width - 5*buttonsize/2, 2*buttonsize, 5*buttonsize/2, 4*buttonsize/2, buttonsize/2);
	cols = width/videoScale;  
	rows = height/videoScale;  
	background(0);
	video = new Capture(this, cols, rows);
	video.start();
}

void captureEvent(Capture video) {  
  	video.read();
}

void draw() {
	if (myImage == null){
		textArr.clear();
		text = null ; 
		// if(text!=null){
		// 	text.fontButton.notOnState();
		// 	text.colorButton.notOnState();
		// }
		camera();
		if (cb.isInvisible()){
			save("testImg.png");
			myImage = loadImage("testImg.png");
			cb.visible();
		}else{
			cb.draw();
		}
		
	}else{
		image(myImage,width/2, height/2,width, height);
		cb.draw();
		aa.draw();
		db.draw();
		
		
		for(Text text: textArr){
			text.draw();
		}
		if (text!=null){
			text.sizeSlider().draw();
			text.rotationSlider().draw();
			text.fontButton().draw();
			text.colorButton().draw();
			if (text.fontButton().isOnState()){
				fontselection.draw();
			}	
			if (text.colorButton().isOnState()){
				text.colorselection().draw();
			}
		}
	}
}
void camera(){
	video.start();
	video.loadPixels(); 
	for (int i = 0; i < cols; i++) {    
		// Begin loop for rows    
		for (int j = 0; j < rows; j++) {      
		// Where are you, pixel-wise?      
		int x = i*videoScale;      
		int y = j*videoScale;
		color c = video.pixels[i + j*video.width];
		fill(c);   
		noStroke(); 
		rectMode(CORNER); // 잔상 지움!!!   
		rect(width- x, y, videoScale, videoScale);    
		}  
	}
}

void mousePressed()
{
	if (cb.isOver()) {
		if (myImage == null){ // 사진 찍기
			cb.invisible();
		}else{ // 다시 찍기
			myImage = null;
		}
  	}
	
	if (aa.isOver()) {
		if (text!=null){
			if (text.colorButton().isOnState()) text.colorButton().notOnState();
			if (text.fontButton().isOnState()) text.fontButton().notOnState();
		}
		
		aa.textOnState();
	
		for (Text text:textArr){
			text.setDone();
		}
	
		Text newText = new Text("", width/2, height/2, 30, color(0)); 
		if (textArr.size()>0 && textArr.get(textArr.size()-1).length()==0){
			textArr.remove(textArr.size()-1);
		}
		textArr.add(newText);
		print(textArr.size());
		text = newText;
		
	}
	if (text!=null){
		if (text.fontButton().isOver()) {
			if (text.colorButton().isOnState()) text.colorButton().notOnState();
			text.setDone();
			if (!text.fontButton().isOnState()) text.fontButton().onState(); // 닫혀있으면 열고 열려있으면 닫기
			else text.fontButton().notOnState();
		}
		if (text.colorButton().isOver()) {
			text.setDone();
			if (text.fontButton().isOnState()) text.fontButton().notOnState();
			if (!text.colorButton().isOnState()) text.colorButton().onState(); // 닫혀있으면 열고 열려있으면 닫기
			else text.colorButton().notOnState();
		}
		for (int i=0; i<fonts.size();i++){
			if(text.fontButton().isOnState() && fonts.get(i).isOver()){
				text.setFont(fonts.get(i).getFont());
			}	
		}
		for (int i=0; i<textArr.size();i++){
			if(textArr.get(i).isOver() && !text.colorselection().isOver() && !text.colorButton().isOver() && !text.fontButton().isOver()){
				text = textArr.get(i);
				for (Text exisitngtext:textArr){
					if (exisitngtext != text){
						exisitngtext.setDone();
					}
				}
			}	
		}
	}	
}

void mouseClicked() {
  int count = mouseEvent.getClickCount();
//   println(count);  // count is 2 for double click
  if (count==2){
	  for (int i=0; i<textArr.size();i++){
		if(textArr.get(i).isOver()){
			text = textArr.get(i);
			text.keepWriting();
			for (Text exisitingtext:textArr){
				if (exisitingtext != text){
					exisitingtext.setDone();
				}
			}
		}	
	}
  }
}

void keyPressed()
{
	if (aa != null && aa.onState){
		
		if (key == ENTER) {  
			text.setDone();  
			if (text.length()==0){
				int index = getIndex(text, textArr);
				textArr.remove(index);
			}  
			return;
		}
		if (key == BACKSPACE) {
			if (text.length() > 0) {
				text.setContent(text.getContent().substring(0, text.length() - 1));
			}
			return;
		}
		if (text.length()==0){
			text.setContent(Character.toString(key));
		}else{
			if (!text.ifDone()) text.setContent(text.getContent()+key); 
		}
	}
}

int getIndex(Text text, ArrayList<Text> textArr)
{
	int index = 0;
	for (int i=0; i<textArr.size(); i++){
		if(text==textArr.get(i)){
			index = i;
			break;
		}
	}
	return index;
}
class Slider
{
	private float x, y_bar, circle, w, h;
	private float startingPosition;
	private int r;
	private color c;
	private boolean vertical;
	private boolean onState;

	Slider(float x, float y_bar, color c, boolean vertical, boolean fromtheLowest){
		this.vertical = vertical;
		r= 10;
		this.x = x;
		this.y_bar = y_bar;
		this.c =c;
		if (vertical){
			w=10;
			h=3*height/5;
			startingPosition = y_bar+h/2-r/2;
			if (fromtheLowest) circle = startingPosition;
			else circle = startingPosition - int((h-r)/13);
		}
		else{
			h=10;
			w = height/5;
			startingPosition = x - w/2+r/2;
			circle = startingPosition;
		}
	}
	// public void setColor()
	public void draw()
	{
		noStroke();
		fill(255);
		rectMode(CENTER);
		rect(x, y_bar, w, h, r );
		fill(c);
		ellipseMode(CENTER);
		if (vertical) ellipse(x, circle, r, r);
		else ellipse(circle, y_bar, r, r);
	}
	public void moveSlider(float p)
	{
		circle = p;
	}
	public boolean isOver()
	{
		if (vertical){
			if (sqrt(sq(mouseX-x)+ sq(mouseY-circle)) > r/2) return false;
			else {
				return true;
			}
		}else{
			if (sqrt(sq(mouseX-circle)+ sq(mouseY-y_bar)) > r/2) return false;
			else {
				return true;
			}
		}
	}
}

abstract class CircleButton{
	private boolean onState;
	private int x, y, r;
	CircleButton(int x, int y, int r){
		this.x = x;
		this.y = y;
		this.r = r;
		onState = false;
	}
	public void draw()
	{
		noStroke();
		ellipseMode(CENTER);
		if (this.isOver()) fill(255, 100, 100);
		else noFill();
		ellipse(x, y, r, r);
	}
	public boolean isOver(){
		if (sqrt(sq(mouseX-x)+ sq(mouseY-y)) > r/2) return false;
		else {
			return true;
		}
	}	
}

abstract class SquareButton{
	private boolean onState;
	private int x, y, d;
	private color c;
	SquareButton(int x, int y, int d, color c){
		this.x = x;
		this.y = y;
		this.d = d;
		this.c = c;
		onState = false;
	}
	public void draw()
	{
		stroke(0);
		strokeWeight(1);
		rectMode(CENTER);
		if (this.isOver()) fill(255, 100, 100);
		else fill(c);
		rect(x, y, d, d);	
	}
	public boolean isOver(){
		if ((Math.abs(mouseX-this.x) > this.d/2) || (Math.abs(mouseY-this.y) > this.d/2)) return false;
		return true;
	}	
	public void onState(){
		onState = true;
	}
	public void notOnState(){
		onState = false;
	}
	public boolean isOnState(){
		return onState;
	}
}

class FontButton extends SquareButton
{
	private boolean onState;
	private PImage icon;
	FontButton(int x, int y, int d, color c){
		super(x, y, d, c);
		icon = loadImage("f.png");
	}
	public void draw(){
		fill(super.c);
		super.draw();
		imageMode(CENTER);
		int iconSize1 = 3* super.d/5;
		int iconSize2 = super.d;
		image(icon, super.x, super.y, iconSize1, iconSize2);
	}
	public void onState(){
		super.onState();
	}
	public void notOnState(){
		super.notOnState();
	}
	public boolean isOnState(){
		return super.isOnState();
	}
}

class ColorButton extends SquareButton
{
	private boolean onState;
	ColorButton(int x, int y, int d, color c){
		super(x, y, d, c);
	}
	public void draw(){
		super.draw();
	}
	public void onState(){
		super.onState();
	}
	public void notOnState(){
		super.notOnState();
	}
	public boolean isOnState(){
		return super.isOnState();
	}
	public void setR(int r){
		float g = green(super.c);
		float b = blue(super.c);
		super.c = color(r, g, b);
	}
	public void setG(int g){
		float r = red(super.c);
		float b = blue(super.c);
		super.c = color(r, g, b);
	}
	public void setB(int b){
		float r = red(super.c);
		float g = green(super.c);
		super.c = color(r, g, b);
	}
}

class CameraButton extends CircleButton
{
	private boolean onState;
	private PImage icon;
	private int original;
	CameraButton(int x, int y, int r){
		super(x, y, r);
		icon = loadImage("cameraIcon.png");
		original = r;
	}
	public void draw(){
		super.draw();
		imageMode(CENTER);
		int iconSize = 3* super.r/5;
		image(icon, super.x, super.y ,iconSize, iconSize);
	}
	public void invisible(){
		super.r=0;
	}

	public boolean isInvisible(){
		return super.r==0;
	}

	public void visible(){
		super.r=original;
	}

	public int size(){
		return super.r;
	}
}

class TextButton extends CircleButton
{
	private boolean onState;
	private PImage icon;

	
	TextButton(int x, int y, int r){
		super(x, y, r);
		icon = loadImage("Aa.png");
	}

	public void draw()
	{
		super.draw();
		int iconSize = 3* super.r/5;
		image(icon, super.x, super.y ,iconSize, iconSize);
		
	}	
	public void textOnState(){
		this.onState = true;
	}
	public boolean textActivate(){
		return onState;
	}
	
}
class DeleteButton extends CircleButton
{
	private boolean onState;
	private PImage icon;
	DeleteButton(int x, int y, int r){
		super(x, y, r);
		icon = loadImage("trashcan.png");
	}
	public void draw()
	{
		noStroke();
		ellipseMode(CENTER);
		if (this.isOver() && text.firstXposition!=-1 && text.firstYposition!=-1) fill(255, 100, 100); // 텍스트를 드래그하다가 지나간 경우만 색깔 바뀌게
		else noFill();
		ellipse(super.x, super.y, super.r, super.r);
		imageMode(CENTER);
		int iconSize = 3* super.r/5;
		image(icon, super.x, super.y ,iconSize, iconSize);
	}	
}

class EachFont extends SquareButton
{
	private boolean onState;
	private PFont font;
	
	EachFont(int x, int y, int d, String font){
		super(x, y, d, color(255));
		if (font.equals("BlackoakStd.otf")) this.font = createFont(font, int(super.d/3));
		else this.font = createFont(font, int(3*super.d/5));
	}
	public void draw(){
		super.draw();
		textAlign(CENTER);
		textFont(font);
		fill(0);
		text("Aa", super.x, super.y); 
	}
	public PFont getFont(){ return font; }
}

class FontSelection
{
	private int x, y, w, h, d;
	private boolean onState;
	private PFont font;
	
	FontSelection(int x, int y, int w, int h, int d){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.d = d;
		for (int i=0 ; i<fontList.length; i++){
			int x1 = int(x-2*d + (i%5)*d);
			int y1 = int(y-3*d/2 + (i/5)*d);
			EachFont eachFont = new EachFont(x1, y1, d, fontList[i]);
			fonts.add(eachFont);
		}
	}
	public void draw(){
		stroke(0);
		strokeWeight(1);
		noFill();
		rectMode(CENTER);
		rect(x, y, w, h);
		for (EachFont eachfont: fonts){
			eachfont.draw();
		}
	}
	public boolean isOver(){
		if ((Math.abs(mouseX-this.x) > this.w/2) || (Math.abs(mouseY-this.y) > this.h/2)) return false;
		return true;
	}
}

class ColorSelection
{
	private int x, y, w, h;
	private boolean onState;
	private Slider rSlider;
	private Slider gSlider;
	private Slider bSlider;
	
	ColorSelection(int x, int y, int w, int h){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		rSlider = new Slider(x, y-buttonsize/2, color(255,0,0), false, true);
		gSlider = new Slider(x, y, color(0, 255, 0), false, true);
		bSlider = new Slider(x, y+buttonsize/2, color(0,0,255), false, true);
	}
	public void draw(){
		stroke(0);
		strokeWeight(1);
		fill(100);
		rectMode(CENTER);
		rect(x, y, w, h);
		// rect(x, y, 5*buttonsize/2, 2*buttonsize);
		rSlider.draw();
		gSlider.draw();
		bSlider.draw();	
	}
	public boolean isOver(){
		if ((Math.abs(mouseX-this.x) > this.w/2) || (Math.abs(mouseY-this.y) > this.h/2)) return false;
		return true;
	}	
	public Slider rSlider(){ return rSlider;}
	public Slider gSlider(){ return gSlider;}
	public Slider bSlider(){ return bSlider;}
}

class Text
{
	private PFont font;
	private float x, y;
	private String content;
	private int size;
	private color c;
	private boolean onState;
	private boolean ifDone;
	private float sw;
	private Slider slider;
	private Slider rotationSlider;

	private float firstXposition;
	private float firstYposition;
	private float textX;
	private float textY;
	private boolean sliderOn;
	private boolean sliderOn2;
	private boolean[] sliderOn3;
	private float rotation;

	private float originalposition;
	private float originalposition2;
	private float[] originalposition3;

	private ColorSelection colorselection;
	private FontButton fontButton;
	private ColorButton colorButton;

	Text(String content,float x, float y, int size, color c){
		this.content = content;
		this.x = x;
		this.y = y;
		this.size = size;
		this.c = c;
		this.onState = true; //쓰는중인지
		this.rotation = 0;
		slider = new Slider(width-2*buttonsize/3, 13*height/24, color(0), true, false);
		rotationSlider = new Slider(width-buttonsize/3, 13*height/24, color(0), true, true);
		this.font = createFont("LithosPro-Regular.otf", 30);

		int a = width - buttonsize/2;
		int b = buttonsize;
		colorselection = new ColorSelection(width - 5*buttonsize/2, 2*buttonsize, 5*buttonsize/2, 2*buttonsize);
		fontButton = new FontButton(int(a-buttonsize/4), int(b+3*buttonsize/4), int(buttonsize/3), color(255));
		colorButton = new ColorButton(int(a+buttonsize/4), int(b+3*buttonsize/4), int(buttonsize/3), color(0));

		firstXposition = -1;
		firstYposition = -1;
		textX = -1;
		textY = -1;
		sliderOn = false;
		sliderOn2 = false;
		sliderOn3 = new boolean[]{false, false, false};
		originalposition = 0;
		originalposition2 = 0;
		originalposition3 = new float[]{0, 0, 0};
	}
	public FontButton fontButton(){ return fontButton; }

	public ColorButton colorButton(){ return colorButton; }

	public void setFont(PFont font){ this.font = font; }

	public ColorSelection colorselection(){ return colorselection; }

	public void keepWriting() { onState = true; }

	public void setDone() { onState = false; }

	public boolean ifDone() { return !onState; }

	public String getContent() { return content; }

	public float getStringWidth() { return sw;}

	public int length() { return content.length(); }

	public void setSize(int s) { size = s; }

	public void setRotation(float a){ rotation = a;}

	public int size() { return size; }

	public void setContent(String update){ this.content = update; }

	public void changePosition(float x, float y){
		this.x = x;
		this.y = y;
	}

	public void setR(int r){
		float g = green(c);
		float b = blue(c);
		this.c = color(r, g, b);
	}
	public void setG(int g){
		float r = red(c);
		float b = blue(c);
		this.c = color(r, g, b);
	}
	public void setB(int b){
		float r = red(c);
		float g = green(c);
		this.c = color(r, g, b);
	}

	public void cursor(color c, float x, float y)
	{
		if ((millis()/500)%2==0) noStroke();
		else {
			strokeWeight(2);
			stroke(c);
		} 
		line(x, y-size, x, y);
	}

	public void draw()
	{
		textFont(font);
		fill(c);
		if (content == "" && onState){
			cursor(c, x, y);
		}else{
			textSize(size);
			textAlign(CENTER);
			sw = textWidth(content);
			if (! onState && rotation!=0){
				pushMatrix();
				translate(x,y);
				rotate(rotation);
				text(content, 0, 0);
				popMatrix(); 
			}else{
				text(content, x, y);
			}
			if (onState){
				cursor(c, x+sw/2, y);
			}
		}
	}	
	public void textRotation(int angle) {rotation = angle; }
	public Slider sizeSlider(){ return slider; }
	public Slider rotationSlider(){ return rotationSlider; }
	public boolean isOver()
	{
		boolean isOver;
		pushMatrix();
		translate(x,y);
		rotate(rotation);
		float mx = mouseX-x;
		float my = mouseY-y;
		if (Math.abs(my*sin(rotation)+mx*cos(rotation))<sw/2 && Math.abs(my*cos(rotation)-mx*sin(rotation))<size/2){
			isOver = true;
		}else{
			isOver = false;
		}
		popMatrix();
		return isOver;	
	}
}

public void mouseDragged() 
{
	if (text != null){
		if (!text.sliderOn && !text.sliderOn2 && !text.sliderOn3[0] && !text.sliderOn3[1] && !text.sliderOn3[2]){
			if (text.ifDone() && text.isOver() && !text.colorselection.isOver() && !fontselection.isOver()){
				if (text.colorButton().isOnState()) text.colorButton().notOnState();
				if (text.fontButton().isOnState()) text.fontButton().notOnState();
				if (text.firstXposition==-1 && text.firstYposition==-1){
					text.firstXposition = mouseX;
					text.firstYposition = mouseY;
					text.textX = text.x;
					text.textY = text.y;
				}
			} 
			if (text.firstXposition!=-1 && text.firstYposition!=-1 && mouseX>0 && mouseX<width && mouseY>0 && mouseY<height){
				float diffX = text.firstXposition-text.textX;
				float diffY = text.firstYposition-text.textY;
				text.sliderOn = false;
				text.sliderOn2 = false;
				text.changePosition(mouseX-diffX, mouseY-diffY);
			}
		}
		
		if (text.sizeSlider().isOver())
		{
			text.colorButton().notOnState();
			text.fontButton().notOnState();
			text.sliderOn = true;
			text.originalposition = text.sizeSlider().circle;
		}
		if (text.sliderOn && mouseY >= text.sizeSlider().y_bar-text.sizeSlider().h/2 +text.sizeSlider().r/2 && mouseY<text.sizeSlider().startingPosition)
		{
			text.sizeSlider().moveSlider(mouseY);
			int updatedSize = 20 + int((text.sizeSlider().startingPosition-mouseY)/(text.sizeSlider().h - text.sizeSlider().r) * 130);
			text.setSize(updatedSize);
		}

		if (text.ifDone() && text.rotationSlider().isOver())
		{
			text.colorButton().notOnState();
			text.fontButton().notOnState();
			text.sliderOn2 = true;
			text.originalposition2 = text.rotationSlider().circle;
		}
		if (text.ifDone() && text.sliderOn2 && mouseY >= text.rotationSlider().y_bar-text.rotationSlider().h/2+text.rotationSlider().r/2 && mouseY <= text.rotationSlider().startingPosition)
		{
			text.rotationSlider().moveSlider(mouseY);
			float updatedRotation = radians((text.rotationSlider().startingPosition-mouseY)/(text.rotationSlider().h - text.rotationSlider().r) * (360));
			text.setRotation(updatedRotation);
		}
		if (text.colorButton().isOnState()){
			if (text.colorselection().rSlider().isOver())
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.sliderOn3[0] = true;
				text.originalposition3[0] = text.colorselection().rSlider().circle;
			}
			if (text.sliderOn3[0] && mouseX <= text.colorselection().rSlider().x + text.colorselection().rSlider().w/2 - text.colorselection().rSlider().r/2 && mouseX >= text.colorselection().rSlider().startingPosition)
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.colorselection().rSlider().moveSlider(mouseX);
				int updatedRed = int((mouseX - text.colorselection().rSlider().startingPosition)/(text.colorselection().rSlider().w - text.colorselection().rSlider().r) * (255));
				text.setR(updatedRed);
				text.colorButton().setR(updatedRed);
			}
			if (text.colorselection().gSlider().isOver())
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.sliderOn3[1] = true;
				text.originalposition3[1] = text.colorselection().gSlider().circle;
			}
			if (text.sliderOn3[1] && mouseX <= text.colorselection().gSlider().x + text.colorselection().gSlider().w/2 - text.colorselection().gSlider().r/2 && mouseX >= text.colorselection().gSlider().startingPosition)
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.colorselection().gSlider().moveSlider(mouseX);
				int updatedGreen = int((mouseX - text.colorselection().gSlider().startingPosition)/(text.colorselection().gSlider().w - text.colorselection().gSlider().r) * (255));
				text.setG(updatedGreen);
				text.colorButton().setG(updatedGreen);
			}
			if (text.colorselection().bSlider().isOver())
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.sliderOn3[2] = true;
				text.originalposition3[2] = text.colorselection().bSlider().circle;
			}
			if (text.sliderOn3[2] && mouseX <= text.colorselection().bSlider().x + text.colorselection().bSlider().w/2 - text.colorselection().bSlider().r/2 && mouseX >= text.colorselection().bSlider().startingPosition)
			{
				text.firstXposition = -1;
				text.firstYposition = -1;
				text.colorselection().bSlider().moveSlider(mouseX);
				int updatedBlue = int((mouseX - text.colorselection().bSlider().startingPosition)/(text.colorselection().bSlider().w - text.colorselection().bSlider().r) * (255));
				text.setB(updatedBlue);
				text.colorButton().setB(updatedBlue);
			}
		}
		
	}
}
public void mouseReleased() {
	if (db!=null && db.isOver() && text!=null && text.firstXposition!=-1 && text.firstYposition!=-1){
		for (int i=0;i<textArr.size();i++){
			if (text == textArr.get(i)){
				textArr.remove(i);
			}
		}
	}
	if (text != null){
		text.firstXposition = -1;
		text.firstYposition = -1;
		text.textX = -1;
		text.textY = -1;
		text.sliderOn = false;
		text.sliderOn2 = false;
		text.sliderOn3[0] = false;
		text.sliderOn3[1] = false;
		text.sliderOn3[2] = false;
	}
}


// your code down here
// feel free to crate other .pde files to organize your code